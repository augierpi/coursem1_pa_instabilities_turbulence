
# install conda and fluidsim

pip2 install mercurial --user

wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh

cat <<EOT >> ~/.bashrc
# added by Pierre Augier
alias ipython="ipython --matplotlib"
export PATH="$HOME/.local/bin/:$HOME/miniconda3/bin/:$PATH"
EOT

source ~/.bashrc

conda install numpy ipython matplotlib h5py cython scipy pandas
pip install pythran colorlog
pip install pyfftw

mkdir -p ~/Dev
cd ~/Dev

hg clone https://bitbucket.org/fluiddyn/fluidfft
cd fluidfft
make
make tests

cd ..
hg clone https://bitbucket.org/fluiddyn/fluidsim
cd fluidsim
make
make tests

mkdir -p ~/Output/Teach
cd ~/Output/Teach
hg clone https://bitbucket.org/paugier/coursem1_pa_instabilities_turbulence
cd coursem1_pa_instabilities_turbulence
make

cd ~/Output/Teach/coursem1_pa_instabilities_turbulence/Pysimul
python job_sim_predaprey.py
