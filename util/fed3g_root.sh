

# apt-get (root)

apt-get update
apt-get dist-upgrade
apt-get install emacs python-pip python-dev libfftw3-dev texlive-latex-base texlive-fonts-extra dvipng
apt-get --purge remove tex.\*-doc$
