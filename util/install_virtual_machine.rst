Install of the virtual machine (lubuntu 17.04)
==============================================

.. code:: bash

   sudo apt install make gcc g++ emacs pip

   pip install mercurial hg-git --user

   wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
   bash Miniconda3-latest-Linux-x86_64.sh

   echo "channels:" > ~/.condarc
   echo "  - conda-forge" >> ~/.condarc

   conda install numpy ipython matplotlib h5py cython jupyter
   conda install scipy

   sudo apt install texlive-latex-base texlive-fonts-extra
   sudo apt-get --purge remove tex.\*-doc$
   sudo apt install dvipng

   pip install pythran colorlog
   
   apt install libfftw3-dev
   pip install pyfftw

   cat <<EOT >> ~/.bashrc
   # added by Pierre Augier
   alias ipython="ipython --matplotlib"
   export PATH="$HOME/.local/bin:$PATH"
   EOT

   hg clone https://bitbucket.org/fluiddyn/fluidfft
   cd fluidfft
   make
   make tests

   cd ..
   hg clone https://bitbucket.org/fluiddyn/fluidsim
   make
   make tests
