---
author:
- Pierre Augier
bibliography:
- './biblio.bib'
title: Instabilities and turbulence
---

This document is a work in progress for a course part of a Master’s
program in Mechanical Engineering at Université Grenoble Alpes (M1). It
is inspired by many books, courses and documents. In particular, I used
a lot the course of Olivier Cadot “Introduction à la turbulence” given
at ENSTA and the book “Hydrodynamique physique” by Guillon, Hulin &
Petit (EPD sciences, denoted GHP in the following).

-   Who I am? Since 2014, I am a CNRS researcher working at LEGI (UGA,
    CNRS) on instabilities and turbulence in stratified and rotating
    fluids, with applications to geophysical fluids, in particular the
    oceans and the atmosphere of the Earth. I do experiments in the
    Coriolis platform and numerical simulations. I did a PhD at LadHyx
    (Paris) and 2 post-docs at KTH (Stockholm) and DAMTP (Cambridge,
    UK).

-   This course is as simple and gradual as possible. You should be able
    to understand everything. Do not hesitate to ask questions!

-   This course has to be interactive! YOU will have to work. With pen
    and paper but also with computers. We will use tools really used in
    sciences and research: in particular Linux, Mercurial
    ([www.mercurial-scm.org](www.mercurial-scm.org)), LaTeX and
    Python...

-   Practical work with Linux. You will need to work with a computer
    with Ubuntu &gt;= 18.04. We will use UGA computers but you can also
    install Ubuntu on your computer. If you have Windows 10, you can use
    the [Windows Subsystem for
    Linux](https://en.wikipedia.org/wiki/Windows_Subsystem_for_Linux) or
    a virtual machine (with a recent version of
    [Virtualbox](https://www.virtualbox.org/wiki/Downloads))

Introduction
============

Instabilities and turbulence: common phenomena and important concepts
---------------------------------------------------------------------

[**Instability**]{} is a very general concept: “when a state is
unstable”. So it is not surprising that instabilities are everywhere: in
flows (what we will study during this course) but also in many other
systems. Here are two examples from the fields of solid mechanics and
fluid-structure interactions:

-   Buckling (for example of columns, see a [video at $t = 40$
    s](https://vimeo.com/132312421)).

-   Suspension bridge flutter and fluid-structure interactions ([see
    this video](https://youtu.be/j-zczJXSxnw)).

A system can be characterized as [**“turbulent”**]{} when structures
with a [**broad range of scales interact non-linearly**]{} in a system
with [**several degrees of freedom**]{}.

Instabilities and turbulence are very general concepts that can be
applied to a lot of systems (populations, ecology, society, climate,
etc.).

Flow instabilities and turbulence are subjects with many practical
interests (vehicles, machines, biological fluids, geophysical fluids,
etc.). They have been widely studied. They are very good models for
research on instabilities and turbulence in general.

![Turbulent wake of vehicles, car and
airfoil.[]{data-label="fig_turbulent_wake_vehicles"}](../Figures/2013_FordFusion_CFDTopandSide "fig:"){width="0.4\linewidth"}
![Turbulent wake of vehicles, car and
airfoil.[]{data-label="fig_turbulent_wake_vehicles"}](../Figures/airfoil_turbulence "fig:"){width="0.55\linewidth"}

#### Two types of descriptions:

mechanisms (instability) and statistics (turbulence).

Reynolds experiment and Reynolds number
---------------------------------------

![Sketch of Reynolds’s dye experiment, taken from his 1883
paper.[]{data-label="fig_Reynolds_exp"}](../Figures/Reynolds_exp){width="0.5\linewidth"}

[Osborne Reynolds
(1842-1912)](https://en.wikipedia.org/wiki/Osborne_Reynolds) carried out
experiments on the flow in a long tube. A drawing of Reynolds and its
experimental setup is shown in figure \[fig\_Reynolds\_exp\].

This [Video of a recent
experiment](https://www.youtube.com/watch?v=xiX5PfFxmIs) helps to
understand what appends.

Water flow through a tube and a small flow volume of ink is injected at
the beginning of the tube.

-   For low velocity, the flow is laminar and the filament of ink stays
    linear along the tube.

-   For larger velocity the flow becomes unstable and there is a
    transition to turbulence. The filament of ink is perturbed and there
    is mixing.

More precisely, depending on the velocity and tube radius, the flow can
be:

-   laminar (straight not perturbed ink filament),

-   perturbed,

-   turbulent (fast mixing).

This flow exhibits a hysteresis! The instability is said
“[*subcritical*]{}”.

When the tube is long enough, the length of the tube is not a relevant
parameter and there are only three relevant physical quantities:

-   viscosity of the water $\nu$ (in m$^2$/s),

-   diameter of the tube $D$ (in m),

-   velocity of the water $U$ (in m/s).

We see that there are only 2 units and 3 variables so we can only form 1
non-dimensional number ([in application of the $\Pi$
theorem](https://en.wikipedia.org/wiki/Buckingham_%CF%80_theorem)):
$$Re = \frac{U D}{\nu}.$$

This is the Reynolds number. It is one of the most important
non-dimensional numbers in fluid dynamics.

Navier-Stokes equations: diffusion and advection
------------------------------------------------

The Navier-Stokes equations for an incompressible
(${\boldsymbol{\nabla}}\cdot {\boldsymbol{v}}= 0$) fluid can be written
as
$${\partial}_t {\boldsymbol{v}}+ {\boldsymbol{v}}\cdot {\boldsymbol{\nabla}}{\boldsymbol{v}}= -{\boldsymbol{\nabla}}p + \nu {\boldsymbol{\nabla}}^2 {\boldsymbol{v}},$$
where ${\boldsymbol{v}}$ is the velocity, $p$ is the rescaled pressure
(in m$^2$/s$^{2}$), $\nu$ is the cinematic viscosity (in m$^2$/s) and
${\boldsymbol{\nabla}}^2$ is the Laplacian operator.

[**IMPORTANT:**]{} You have to know by heart these equations. To be able
to write them without thinking too much in vectorial form and to really
understand this notation (for example
${\boldsymbol{v}}\cdot {\boldsymbol{\nabla}}{\boldsymbol{v}}|_i = v_j {\partial}_j v_i$).
You have to be able to define all quantities (${\boldsymbol{v}}$, $p$,
$\nu$, $t$, ${\boldsymbol{\nabla}}$) and to know their units. You have
to know and understand the physical meaning of all terms.

#### Diffusive transport

The last term of the NS equation is the same that the term appearing in
the temperature equation describing the heat diffusion:
$${\partial}_t T = \kappa {\boldsymbol{\nabla}}^2 T.$$ The diffusive
time is $\tau_\nu = L^2/\nu$.

#### Advective transport

The Euler equation describes the dynamics of an incompressible perfect
fluid (without viscosity):
$${\partial}_t {\boldsymbol{v}}+ {\boldsymbol{v}}\cdot {\boldsymbol{\nabla}}{\boldsymbol{v}}= -{\boldsymbol{\nabla}}p$$

The advective term is nearly the same in the equation of advection of a
field $f(x, t)$ by a constant velocity $U$:
$${\partial}_t f(x,t) + U {\partial}_x f(x, t) = 0.$$

Notation for the advective term:
${\partial}_t {\boldsymbol{v}}+ {\boldsymbol{v}}\cdot {\boldsymbol{\nabla}}{\boldsymbol{v}}= {\text{D}_t}{\boldsymbol{v}}$.

Remark vocabulary: advection and convection.

The advective time is $\tau_a = L/U$.

#### Scaling analysis of the Navier-Stokes equation

First step: characteristic values for the velocity $U$, the length scale
$L$ and the pressure $P$. We can define nondimensional variables as:
$${\boldsymbol{v}}' = {\boldsymbol{v}}/ U, \quad {\boldsymbol{x}}' = {\boldsymbol{x}}/ L, \quad
t' = t / (L/U) \text{ and } p' = p / P.$$

Let’s compute the scaling of the different terms of the incompressible
Navier-Stokes equations. After some equations (white board), we get the
scaling for the pressure: $P \sim U^2$.

We obtain the nondimensional incompressible Navier-Stokes equations:
$${\partial}_{t'} {\boldsymbol{v}}' + {\boldsymbol{v}}' \cdot {\boldsymbol{\nabla}}' {\boldsymbol{v}}' = -{\boldsymbol{\nabla}}' p' + \frac{1}{Re} {\boldsymbol{\nabla}}'^2 {\boldsymbol{v}}',$$
with $Re = UL/\nu$.

The Reynolds number can be written as a ratio of forces:
$$Re = \frac{|{\boldsymbol{v}}\cdot {\boldsymbol{\nabla}}{\boldsymbol{v}}|}{|\nu {\boldsymbol{\nabla}}^2 {\boldsymbol{v}}|}.$$
or of time scales:
$$Re = \frac{UL}{\nu} = \frac{\frac{L^2}{\nu}}{\frac{L}{U}} =
\frac{\tau_\nu}{\tau_a}.$$

An example: the wake behind a cylinder
--------------------------------------

Figure \[fig\_wake\_cylinder\_regimes\] show four pictures of the flow
behind a cylinder. We see that these four states are qualitatively very
different, from laminar (regular, steady, symmetrical) to nearly
turbulent. Figure \[fig\_wake\_cylinder\_turb\] presents a really
turbulent case. [This
video](https://www.youtube.com/watch?v=JI0M1gVNhbw) (at $t = 3$ min)
shows a clear example of the vortex shedding behind a cylinder for
different velocities (Reynolds numbers).

Let’s recall and introduce very important concepts:

-   For this flow, the control parameter is also the Reynolds number
    $Re$.

-   Instabilities. Stable and unstable states.

-   Breaking of symmetry (space and time).

-   Vortex shedding with a characteristic frequency: Strouhal number
    $St = fD / U \simeq 0.2$...

-   Transition to turbulence (see figure \[fig\_wake\_cylinder\_turb\]).

-   For this configuration there is no hysteresis. This is a
    “*supercritical*” instability.

![Visualization of a flow behind a cylinder for different Reynolds
numbers. (a) $Re = 1.54$; (b) $Re = 26$; (c) $Re = 200$; (d)
$Re = 8000$. Taken from
GHP.[]{data-label="fig_wake_cylinder_regimes"}](../Figures/wake_cylinder_regimes){width="0.75\linewidth"}

![Visualization of the turbulent wake of a cylinder at large Reynolds
number.[]{data-label="fig_wake_cylinder_turb"}](../Figures/turb_wake_cylinder){width="0.6\linewidth"}

Remark: fluid-structure interaction... See [this
video](https://www.youtube.com/watch?v=_Hbbkd2d3H8).

Questions
---------

-   Write the vectorial version of the Navier-Stokes equations for an
    incompressible fluid.

-   Give a definition of the Reynolds number as a ratio of two terms of
    the Navier-Stokes equations.

-   Give the characteristic time of a diffusive process along a length
    $L$.

-   Give the characteristic time of a advective process along a length
    $L$.

-   What is the Strouhal number? What is its typical value for the wake
    of a cylinder?

Instabilities and transition to turbulence
==========================================

We will first present important concepts about instabilities considering
very simple and idealized systems with few degrees of freedom. We will
then see how these concepts can be used to understand flow dynamics and
how instabilities can lead to disordered flows.

Systems with few degrees of freedom
-----------------------------------

### Examples with 1 degree of freedom

#### Non-linear rigid pendulum

 \
We consider the very simple mechanical system of a mass hung to a rigid
bar free to oscillate in one dimension. Let’s start by an exercise:
prove that the dynamics of this system can be describe by this equation:
$$\ddot \theta =  - \Omega_g^2 \sin\theta - 2 f_d \dot\theta,$$ where
$\Omega_g = \sqrt{g/R}$ and $f_d$ is a dissipation frequency.

Fixed points are points (values of the variables) for with $\dot\theta =
0$. What are the fixed points of this equations?

Answer: The two fixed points are $\theta_0 = 0$ and $\theta_1 = \pi$.

To study the [**stability of a fixed point**]{}, we have to linearize
the equation around this fixed point. We write the variable as the sum
of a fixed points value plus a small perturbation:
$\theta = \theta_f + {\varepsilon}$ and we neglect the nonlinear terms
involving ${\varepsilon}^2$. We then can look for a solution of the
obtained linear equation in the form
${\varepsilon}\propto e^{\sigma t}$. We then obtain an equation for the
growth rate of the perturbation $\sigma$.

[**First fixed point, $\theta_0 = 0$:**]{}

$$\sigma^2 + 2 f_d\sigma + {\Omega_g}^2 = 0$$

$\sigma_\pm = -f_d \pm \sqrt{f_d^2 - \Omega_g^2}$

Case $f_d \ll \Omega_d$: $\sigma_{\pm r} \simeq - f_d < 0$ and
$\sigma_{\pm i} = \pm \Omega_g$.

Stable...

[**Second fixed point, $\theta_1 = \pi$:**]{}

$$\sigma^2 + 2 f_d\sigma - {\Omega_g}^2 = 0$$

One unstable mode...

#### Non-linear rotating pendulum

 \
Let’s consider the same mechanical system but with a global system
rotation. A picture of an experiment of such rotating pendulum is shown
in Figure \[fig\_pendulum\].

![Picture of an experiment of a rotating
pendulum.[]{data-label="fig_pendulum"}](../Figures/rotating_pendulum){width="0.4\linewidth"}

Try to show that the equation describing the dynamics of the rotating
pendulum can be written as
$$\ddot \theta =  - \Omega_g^2 \sin\theta + \Omega^2\sin\theta \cos\theta.$$

We now have three fixed points: $\theta_0 = 0$, $\theta_1 = \pi$ and
$\theta_\pm = \pm \arccos(\Omega_g^2 / \Omega^2)$ (only if
$\Omega > \Omega_g$).

[**Stability of the first fixed point $\theta_0 = 0$:**]{}

$\theta = \theta_0 + \theta'$.

$\sigma_0^2 = \Omega^2 - \Omega_g^2$.

[**Stability of the other fixed point
$\cos \theta_\pm = (\Omega_g / \Omega)^2$:**]{}

We can write the governing equation as:

$$\ddot \theta = \sin\theta \Omega^2 (\cos\theta - \cos \theta_\pm)$$

We write the angle as $\theta = \theta_\pm + \theta'$ and we develop
$\cos\theta$ and $\sin\theta$ around $\theta_\pm$: $$\begin{aligned}
\cos(\theta_\pm + {\varepsilon}) &= \cos\theta_\pm - \sin\theta_\pm {\varepsilon}, \\
\sin(\theta_\pm + {\varepsilon}) &= \sin\theta_\pm + \cos\theta_\pm {\varepsilon}.\end{aligned}$$
By using the trigonometry equality
$\sin^2\theta_\pm = 1 - \cos^2\theta_\pm$, we find
$$\Big(\frac{\sigma_\pm}{\Omega_g}\Big)^2 =
\Big(\frac{\Omega_g}{\Omega}\Big)^2 - \Big(\frac{\Omega}{\Omega_g}\Big)^2$$
Therefore, these fixed points are unstable when $\Omega < \Omega_g$ and
stable (oscillations) when $\Omega > \Omega_g$.

[**Close to the transition $\tilde \Omega \ll 1$**]{}

Non dimensional control parameter:
$$\tilde \Omega = \frac{\Omega - \Omega_g}{\Omega_g}$$

$\Omega / \Omega_g = 1 + \tilde \Omega$.

In the limit $\tilde \Omega \ll 1$, we have
$\cos\theta_\pm = (\Omega / \Omega_g)^{-2} \Rightarrow 1 - \theta^2 / 2 = 1 - 2
\tilde \Omega \Rightarrow \theta_\pm \simeq \pm 2 \sqrt{\tilde \Omega}$.

We can plot the transition diagram close to the bifurcation. This is a
[**Pitchfork bifurcation**]{}.

![Supercritical pitchfork bifurcation: solid lines represent stable
points and the dashed line represents an unstable
one.[]{data-label="fig_pitchfork"}](../tmp/fig_pitchfork_bifurcation){width="0.55\linewidth"}

#### Introduction of the concepts of supercritical and subcritical instabilities

 \
Figure \[fig\_supersubcritical\] show two diagrams corresponding to
supercritical and subcritical bifurcations, respectively. The first
bifurcation of the wake of a cylinder is supercritical (similar to the
rotating pendulum). The transition to turbulence in a straight tube is
due to a subcritical instability (hysteresis).

![Bifurcation diagrams for a supercritical instability (left) and a
subcritical instability
(right).[]{data-label="fig_supersubcritical"}](../tmp/fig_supersub){width="0.9\linewidth"}

### Example with 2 degrees of freedom (linearization, diagonalization, eigenmodes)

We consider the predator–prey model, also known as the Lotka–Volterra
equations: $$\begin{aligned}
\dot X &= A X - B XY, \\
\dot Y &= -C Y + D XY,\end{aligned}$$ where $X(t)$ and $Y(t)$ are two
functions of time representing species of preys and predators,
respectively. The parameters $A$, $B$, $C$ and $D$ are real and
positive. This model is very simple, which will help us to introduce the
concepts of [**fixed points**]{}, [**linearization**]{},
[**diagonalization**]{} of a linear system, [**eigenvalues**]{} and
[**eigenmodes**]{}.

#### Fixed points

 \
A first simple step when studying a dynamical system consists in finding
fixed points, i.e. points for which the system is stationary
($(\dot X,\ \dot Y) = (0,\ 0)$). To find the fixed points, let’s
factorize the system of equation: $$\begin{aligned}
\dot X &= X (A - B Y), \\
\dot Y &= -Y(C - D X).\end{aligned}$$ The first trivial fixed point is
$(X,\ Y) = (0,\ 0)$ and corresponds to the extinction of the two
species. We see that there is also a non-trivial fixed point:
$(X,\ Y) = (C/D,\ A/B)$.

#### Stability of the fixed points

 \
A second step is to study the stability of the two fixed points. We
first have to linearise the equations around a fixed point. We consider
an infinitesimal perturbation so we write the solution in the form
$X = X_f + x$ and $Y = Y_f + y$.

[**For the first fixed point,**]{} it is trivial. The linearized
equations are $$\begin{aligned}
\dot x &= Ax, \\
\dot y &= -Cy,\end{aligned}$$ and can be written as $\dot V = J V$, with
$$V =
  \begin{pmatrix}
    x \\
    y
  \end{pmatrix}
\quad\text{and}\quad
J =
  \begin{pmatrix}
    A & 0 \\
    0 & -C
  \end{pmatrix}.$$ $J$ is the Jacobian matrix of the system. We need to
find eigenvalues and eigenmodes of this matrix, i.e. vector $V_i$ such
that $\sigma_i V_i = J
V_i$. Since, $J$ is in this case already diagonal, it is trivial. The
eigenvalues are $\sigma_1 = A$ and $\sigma_2 = -C$ and the eigenvectors
are $$V_1 =
  \begin{pmatrix}
    1 \\
    0
  \end{pmatrix}
\quad\text{and}\quad
V_2 =
  \begin{pmatrix}
    0 \\
    1
  \end{pmatrix}.$$ Since $A$ and $C$ are both positive, this fixed point
is unstable and it is a saddle point ($\sigma_1 > 0$ and
$\sigma_2 < 0$).

[**We now consider an infinitesimal perturbation around the second fixed
point $(X,\ Y) = (C/D,\ A/B)$**]{}. The linearized equations can be
written as $\dot V = J V$ with the Jacobian matrix $$J =
  \begin{pmatrix}
    0 & -\frac{CB}{D} \\
    \frac{AD}{B} & 0
  \end{pmatrix}.$$ This time, it is not diagonal so we have to work a
little bit more. The solvability condition is
$$\det (J - \sigma_i {\boldsymbol{1}}) = 0,$$ with ${\boldsymbol{1}}$
the identity matrix, which gives us $\sigma_\pm = \pm i \sqrt{AC}$,
which means that the fixed point is elliptic (characterizes the shape of
the orbits close to the fixed point). By injecting the value of the
eigenvalue in the linearized equations, we find the eigenvectors:
$$V_\pm =
  \begin{pmatrix}
    \pm i \frac{B}{D}\sqrt{\frac{C}{A}} \\
    1
  \end{pmatrix}.$$

[**Phase space:**]{} place the two fixed points in the space $(X,\ Y)$.
Plot (approximately) some trajectories.

[**Conserved quantity:**]{} Check that the quantity
$$U(X,\ Y) = C \log(X) -DX + A \log(Y) - BY = \text{constant}$$ is
conserved along the trajectories.

[**Periodic orbits and predictability:**]{} The orbits are periodic. The
state of the system remains predictable for all times. There is no
sensitivity to initial conditions.

### Example with 3 degrees of freedom : the Lorenz model (chaos)

We have just studied a deterministic system with 2 degrees of freedom.
We have seen that the system is predictable.

We are going to see that something very different happens for a very
similar deterministic system with just 1 more degree of freedom, the
Lorenz model inspired by the convection in the atmosphere:
$$\begin{aligned}
\dot X &= \sigma (Y - X),\\
\dot Y &= \rho X - Y - XZ.\\
\dot Z &= X Y - \beta Z.\end{aligned}$$

Find the fixed points...

There is no periodic orbit and there is a strong sensitivity to initial
conditions, which is called the “butterfly” effect. This is an example
of chaos for systems characterized by a small number of degrees of
freedom. We see that we do not need many degrees of freedom to get
unpredictable system.

Fluid instability mechanisms and conditions
-------------------------------------------

### Squire’s theorem

Squire’s theorem states that for flows governed by the incompressible
Navier-Stokes equations, the perturbations of a two-dimensional flow
which are least stable are also two-dimensional.

### One-dimensional shear $U(z)$, inflection point theorem

Rayleigh’s equation (inviscid Orr-Sommerfeld equation).

$$\varphi'' - k^2 \varphi - \frac{U''}{U-c} \varphi = 0 \label{rayleigh_eq}$$

#### Derivation

We start from the inviscid Navier-Stokes equation

$${\text{D}_t}{\boldsymbol{v}}= - {\boldsymbol{\nabla}}p$$

Two-dimensional perturbation around a shear:
${\boldsymbol{v}}= (U(z) + u, 0, w)$. $$\begin{aligned}
({\partial}_t + U{\partial}_x) u + w U' &= -{\partial}_x p \\
({\partial}_t + U{\partial}_x) w &= -{\partial}_z p.\end{aligned}$$

We eliminate the pressure:

$$({\partial}_t + U{\partial}_x) ({\partial}_z u - {\partial}_x w) + w U'' = 0$$

This is actually the equation for the $y$ component of the vorticity
$\omega = {\partial}_x w - {\partial}_z u$.

The flow is incompressible and 2d so we can use a streamfunction $\psi$
such as $u = {\partial}_z \psi$ and $w = -{\partial}_x\psi$. The
vorticity is equal to
$\omega = - ({\partial}_{xx} + {\partial}_{zz}) \psi$, which gives:

$$({\partial}_t + U{\partial}_x) ({\partial}_{xx} + {\partial}_{zz}) \psi - {\partial}_x\psi U'' = 0$$

Let’s use the symmetries of the problem with time and $x$ so every
function can be written using the Fourier transform:
$$\psi = {\mathcal{R}}( \varphi e^{ik(x- ct)}),$$ with $k$ a real
wavenumber and $c$ a complex speed. We can replace the operators
${\partial}_t$ and ${\partial}_x$ by $-ikc$ and $ik$, respectively and
we obtain the Rayleigh equation (\[rayleigh\_eq\]).

#### Theorems

$$\int_{-\infty}^{+\infty} dz \Big( \varphi'' \varphi^* - k^2 |\varphi|^2 -
\frac{U''}{U-c} |\varphi|^2 \Big) = 0$$

Integration by part + limit conditions:

$$- \int_{-\infty}^{+\infty} dz \Big( |\varphi'|^2 + k^2 |\varphi|^2 \Big)
= \int_{-\infty}^{+\infty} dz \Big( \frac{U''}{U-c} |\varphi|^2 \Big) < 0$$

The right hand side term can also be written as
$$\int_{-\infty}^{+\infty} dz \Big( \frac{(U-c^*)U''}{|U-c|^2} |\varphi|^2 \Big),$$
so we obtain two conditions
$$c_i \int_{-\infty}^{+\infty} dz \Big( \frac{U''}{|U-c|^2} |\varphi|^2 \Big) = 0,$$
and
$$\int_{-\infty}^{+\infty} dz \Big( \frac{(U-c_r)U''}{|U-c|^2} |\varphi|^2 \Big) < 0$$

We first conclude from the first quantity that such shear can be
linearly unstable only if $U''(z_i)$ changes sign (i.e. if the profile
has an inflection point).

One can infer from the two conditions a more restrictive version that
states that a velocity profile that has an inflection point $z_i$ can be
linearly unstable only if $U''(y) (U(y) - U(y_i)) < 0$.

![Stability for different velocity profiles. There is no inflection
point in (a) and (b) so these profiles are stable. (c) has a inflection
point but is stable since $U''(y) (U(y) - U(y_i)) > 0$. (d) could be
unstable.[]{data-label="fig_rayleigh_profiles"}](../Figures/rayleigh_profiles){width="0.75\linewidth"}

Remark: flow in a tube, Reynolds experiment and sub-critical
instability...

### Centrifugal instability (Rayleigh criterion)

Flows with curved streamlines, such as those sketched in
figure \[fig\_swirling\_flows\], can be unstable due to the centrifugal
force. There is a simple inviscid criterion for the instability of a
basic swirling flow with an arbitrary dependence of angular velocity
$\Omega(r)$ on the distance $r$ from the axis of rotation
($u_\theta = r\Omega$). The centrifugal instability can develop if

$$\Phi(r) < 0 \quad \text{where} \quad
\Phi(r) \equiv \frac{1}{r^3}\frac{d}{dr} \Big( r^4 \Omega^2 \Big). \label{eqRayleighcriterion}$$

![Examples of swirling
flows.[]{data-label="fig_swirling_flows"}](../Figures/examples_swirling_flows){width="0.75\linewidth"}

This criterion can be explained by considering the Euler equations for a
bidimensional ($w=0$ and ${\partial}_z = 0$) incompressible flow
expressed in cylindrical coordinates: $$\begin{aligned}
{\text{D}_t}u_r - \frac{{u_\theta}^2}{r} &= - {\partial}_r p, \label{equr} \\
{\text{D}_t}u_\theta + \frac{u_r u_\theta}{r} &= - \frac{1}{r} {\partial}_\theta p, \label{equtheta}\end{aligned}$$
where the material derivative is
$${\text{D}_t}= {\partial}_t + u_r{\partial}_r + \frac{u_\theta}{r} {\partial}_\theta. \label{eqDtcylindrical}$$

The continuity equation is
$${\partial}_r u_r + \frac{u_r}{r} + \frac{1}{r} {\partial}_\theta u_\theta = 0. \label{eqconticylindrical}$$

The equation for the vorticity is:
$${\text{D}_t}{\boldsymbol{\omega}}= {\boldsymbol{\omega}}\cdot {\boldsymbol{\nabla}}{\boldsymbol{u}}.$$
Show that this leads to a simple equation for the vertical component of
the vorticity ${\text{D}_t}\omega_z$.

##### Exercise: how should we obtain these equations?

#### Energetic argument

Using (\[equtheta\]) and (\[eqDtcylindrical\]), it can be shown that the
angular momentum $H = r u_\theta$ is conserved along the trajectory of a
fluid particle $ {\text{D}_t}(r u_\theta) = 0$.

The kinetic energy per mass unit is equal to $(H/r)^2/2$. We consider
two fluid particles with equal volume. The sum of their kinetic energy
is
$$\frac{2 E_K}{dV} = \frac{{H_1}^2}{{r_1}^2} + \frac{{H_2}^2}{{r_2}^2}.$$
Let’s consider the swaps of the positions of the 2 particles. The new
kinetic energy is
$$\frac{2 E_{K\text{new}}}{dV} = \frac{{H_1}^2}{{r_2}^2} +
\frac{{H_2}^2}{{r_1}^2}.$$ The difference can be written as
$$\frac{2 (E_{K\text{new}} - E_K)}{dV} = ({H_2}^2 - {H_1}^2)
\Big( \frac{1}{{r_1}^2} - \frac{1}{{r_2}^2} \Big).$$ If the swap has
released energy ($\Delta E < 0$, ${H_1}^2 > {H_2}^2$), the laminar base
flow will be unstable to such swaps. Thus the criterion is
$$\frac{dH^2}{dr} < 0 \quad \text{for instability.}$$ Recalling that
$H = r^2 \Omega$, the condition for instability as a function of $r$ and
$\Omega$ is
$$\frac{d}{dr}(r^4 \Omega^2) < 0 \quad \text{for instability,}$$ which
is consistent with the Rayleigh criterion (\[eqRayleighcriterion\]).

Two concepts to go further...
-----------------------------

### Drag coefficient

The ith component of the force on an obstacle can be written as
$$F_i = \iint_\Sigma \sigma_{ij} dS_j.$$

The total power injected in the flow is
$$P = {\boldsymbol{F}}\cdot {\boldsymbol{U}},$$ where ${\boldsymbol{U}}$
is the velocity relative to the object.

We define $S$ as the surface projected on a cross-section perpendicular
to the direction of the flow. Since the strength should scale as
$\rho U^2 S$, we define the drag coefficient (dimensionless) as:
$$C_D = \frac{2F_x}{\rho U^2 S}$$

The exact definition of the surface depends on the shape of the object
(sphere, plate, etc.). The force can be due to pressure (force normal to
the surface) or to friction (viscous force).

![Definition of the surface involved in the definition of the drag
coefficient for different
objects.[]{data-label="fig_drag_coef_def"}](../Figures/drag_coef_def){width="0.6\linewidth"}

For tubes or plates, an alternative quantity $\lambda$ is used to
quantify the drag. The pressure is measured at two different points and
the difference of pressure is a direct measure of the friction:
$$\lambda = \frac{2}{\rho U^2 S} \frac{\Delta P D}{l},$$ where $D$ is
the diameter of the tube. Show that in the case of the tube,
$C_D = \lambda/4$.

![The difference of pressure is measured by the difference of water
height in two
tubes.[]{data-label="fig_drag_coef_def_pressure_drop"}](../Figures/drag_coef_def_pressure_drop){width="0.5\linewidth"}

![Evolution of the drag coefficient with the Reynolds number for a
cylinder. The flow is stationary for small $Re$. The wake becomes
periodic for intermediate
$Re$.[]{data-label="fig_drag_coef_vs_Re_cylinder"}](../Figures/drag_coef_vs_Re_cylinder){width="0.6\linewidth"}

For the cylinder the drag becomes constant for very large Reynolds
number. This implies that the energy dissipation scales like
$\rho U^3/D$ and does not depend on the viscosity.

### Amplitude equations

For some instabilities, it is possible to proposed a model describing
some characteristics of the system close to the threshold of the
instability without considering the fundamental equation of the fluid
mechanics. This approach is called the amplitude equation and is based
on the symmetries of the system.

One of the simplest examples is the case of the cylinder wake (Landau
model, see p. 111 of GHP). We look for the equation describing the
evolution of the complex amplitude of the perturbation $A(t)$. We try
$$d_t |A|^2 = 2\sigma_r |A|^2 - 2B |A|^4,$$ which is equivalent to
$$d_t |A| = \sigma_r |A| - B |A|^3.$$ So a consistent equation for the
complex amplitude can be $$d_t A = \sigma A - B |A|^2 A.$$

From this equation, we find that $|A_{eq}| = \sqrt{\sigma_r / B}$. Since
we know that $\sigma_r = 0$ for $Re = Re_c$, we can use the Taylor
expansion $\sigma_r \propto (Re - Re_c)$, which would imply that
$|A_{eq}| \propto \sqrt{Re - Re_c}$. These relations are consistent with
the measurements.

This model is also consistent with the observation that the
characteristic time of the evolution of the perturbation scales like
$1/(Re - Re_c)$. This time diverges when the Reynolds number approaches
the critical Reynolds number $Re_c$. These characteristics are also
observed in phase transitions, which can be described with the same
Landau model.

Other flows examples
--------------------

### Boundary layer on a flat solid

![image](../Figures/instability_flat_boudary_layer){width="0.45\linewidth"}
\[fig\_instability\_flat\_boudary\_layer\]

### Boundary layer on a curvy solid (centrifugal and detachment)

-   concave curve boundary: potentially unstable to the centrifugal
    instability.

-   convex curve boundary: separation (pressure gradient).

![image](../Figures/separation_boundary_layer){width="0.4\linewidth"}
\[fig\_separation\_boundary\_layer\]

### Vortex instabilities

![Instabilities can develop in the vortices behind an
aircraft.[]{data-label="fig_wake_aircraft"}](../Figures/wake_aircraft "fig:"){width="0.35\linewidth"}
![Instabilities can develop in the vortices behind an
aircraft.[]{data-label="fig_wake_aircraft"}](../Figures/crow_instability "fig:"){width="0.4\linewidth"}

### Poiseuille (plan and tube)

### Couette flow, Taylor-Couette flow

Effects of variable density
===========================

Boussinesq approximation
------------------------

For a compressible fluid, the conservation of mass can be written as:
$${\text{D}_t}{\rho_{\text{tot}}}+ {\rho_{\text{tot}}}{\boldsymbol{\nabla}}\cdot {\boldsymbol{u}}= 0 \Leftrightarrow
{\partial}_t {\rho_{\text{tot}}}+ {\boldsymbol{\nabla}}\cdot({\rho_{\text{tot}}}{\boldsymbol{u}}) = 0.$$

The Navier-Stokes equation is
$${\rho_{\text{tot}}}{\text{D}_t}{\boldsymbol{u}}= -{\boldsymbol{\nabla}}\tilde p + {\rho_{\text{tot}}}{\boldsymbol{g}}+ {\rho_{\text{tot}}}\nu {\boldsymbol{\nabla}}^2 {\boldsymbol{u}}.$$

The density is decomposed in 3 parts:
${\rho_{\text{tot}}}= \rho_0 + \tilde\rho(z) + \rho'$, where the time
average density is $\bar\rho(z) = \rho_0 + \tilde\rho(z)$.

The Boussinesq approximation consists in replacing ${\rho_{\text{tot}}}$
by $\rho_0$ everywhere except in the buoyancy term.
$${\text{D}_t}{\boldsymbol{u}}= -{\boldsymbol{\nabla}}\frac{\tilde p}{\rho_0} + \frac{{\rho_{\text{tot}}}{\boldsymbol{g}}}{\rho_0} +
\nu {\boldsymbol{\nabla}}^2 {\boldsymbol{u}}.$$ and the mass
conservation becomes ${\boldsymbol{\nabla}}\cdot {\boldsymbol{u}}= 0$.

We can subtract to this equation its static solution obtained for
${\boldsymbol{u}}= 0$:
$$0 = -{\boldsymbol{\nabla}}\frac{\tilde p_s}{\rho_0} + \frac{\bar\rho {\boldsymbol{g}}}{\rho_0}$$
which gives
$${\text{D}_t}{\boldsymbol{u}}= -{\boldsymbol{\nabla}}p + {\boldsymbol{b}}+ \nu {\boldsymbol{\nabla}}^2 {\boldsymbol{u}},$$
where $p$ is the rescaled dynamic pressure and ${\boldsymbol{b}}$ is the
buoyancy.

The conservation of internal energy and mass of salt can be rewritten as
$${\text{D}_t}{\rho_{\text{tot}}}= \kappa {\boldsymbol{\nabla}}^2 {\rho_{\text{tot}}},$$
which leads with some assumption to
$${\text{D}_t}b + N^2 u_z = \kappa {\boldsymbol{\nabla}}^2 b,$$ where
$N = \sqrt{d_z \bar b_{\text{tot}}}$ is the Brunt-Väisälä frequency.

[**Do it:**]{} Compute the frequency $\omega$ of a perturbation for a
modified set of equations for which you assume that there is no
horizontal movement. Also assume that the Brunt-Väisälä frequency is
constant and that there is no dissipation ($\nu = \kappa = 0$).

Unstable stratification, Rayleigh-Taylor instability
----------------------------------------------------

When a lighter fluid is placed below a denser fluid, the lighter fluid
can go up and the denser fluid go down. The picture of an experiment on
such instability, called Rayleigh-Taylor, is shown in
figure \[fig\_rayleigh\_taylor\_DaviesWykes\].

![Picture of a Rayleigh-Taylor experiment. Taken from a
[https://youtu.be/NI85oC-3mJ0](video) produced by Davies Wykes and
Dalziel (DAMTP, University of Cambridge,
UK)[]{data-label="fig_rayleigh_taylor_DaviesWykes"}](../Figures/rayleigh_taylor_DaviesWykes){width="0.4\linewidth"}

Rayleigh-Benard instability ($Ra$, $Nu$)
----------------------------------------

Unstable density gradient can be produced by heat. Close to the
transition, this instability leads to the formation of organized
geometric structures.

![image](../Figures/rayleighbenard_pan){width="0.4\linewidth"}
\[fig\_rayleighbenard\_pan\]

The control parameter is the Rayleigh number
$$Ra = \frac{g\beta}{\nu\alpha} \Delta T H^3,$$ where $\beta$ is the
thermal expansion coefficient, $\alpha$ is the thermal diffusivity,
$\Delta T$ is the difference between the bottom and top temperature and
$H$ is the height.

A effect of the development of the Rayleigh-Benard instability is to
increase the heat flux. When the Rayleigh number is large, convection is
more efficient to transport heat than conduction. The heat flux
normalized by the diffusive heat flux is called the Nusselt number $Nu$.

The plate tectonics is due to the convection of the mantle!

Stable stratification, Kelvin-Helmoltz instability and Richardson number
------------------------------------------------------------------------

When the density decreases with altitude, the density profile is stable.
Such stable density profile can influence the flow.

In particular, a shear layer that would be unstable to the shear
instability can become stable because of the density stratification. The
effect of the stratification can be quantified with the Richardson
number $$Ri = \Big(\frac{N}{d_zU}\Big)^2.$$ The condition $Ri < 1/4$
somewhere in the flow is a necessary but not sufficient condition for
the shear instability of a steady parallel inviscid shear flow
[@Miles1961; @Howard1961].

Turbulence
==========

Introduction
------------

### Example of turbulent signals

![Typical turbulent velocity signal $u(t)$ obtained for example with a
hot wire for a very large Reynolds
number.[]{data-label="fig_turb_signal"}](../Figures/turb_signal){width="0.7\linewidth"}

-   Unpredictable (chaos at high number of degrees of freedom).

-   Multi-scale (time and space). Wideband spectra.

-   Large Reynolds number. Strongly nonlinear.

Turbulence study is a lot about analysis and understanding the
characteristics of such flow signal
${\boldsymbol{u}}({\boldsymbol{x}}, t)$. What can we do with such
signals?

-   Average and filtering ${\langle u \rangle}$.

-   Energy ${\langle {\boldsymbol{u}}^2/2 \rangle}$.

-   Time and space correlations. Structure functions.

-   Spectra.

### A simple model: Richardson cascade, dissipation rate $\varepsilon$

At very large Reynolds number, the drag coefficient is constant... This
tends to indicate that the rate of dissipation of the energy is not a
function of the viscosity! The equation for the local kinetic energy per
mass unit is:
$${\text{D}_t}{\boldsymbol{u}}^2/2 = - {\boldsymbol{u}}\cdot{\boldsymbol{\nabla}}p + \nu {\boldsymbol{u}}\cdot {\boldsymbol{\nabla}}^2 {\boldsymbol{u}}.$$
The dissipation term is
${\varepsilon}({\boldsymbol{x}}, t) = \nu {\boldsymbol{u}}\cdot {\boldsymbol{\nabla}}^2 {\boldsymbol{u}}$.
It is proportional to the viscosity so it should tends towards 0 when
$\nu$ tends towards 0, but it is actually constant when $\nu$ tends
towards 0! This surprising limit can be explained only by the fact that
the size of the smallest structures in the flow decreases when $\nu$
tends towards 0 such that the rate of dissipation of the energy is
constant.

![A simple model of turbulence introduced by physicist Lewis Richardson
in 1920. Credit: Antonio
Celani.[]{data-label="fig_Richardson_cascade"}](../Figures/Richardson_cascade){width="0.4\linewidth"}

Average and Reynolds decomposition
----------------------------------

### Decomposition of the flow field

We decompose the flow field using a averaging (or filtering) operator
${\langle . \rangle}$ as:
$${\boldsymbol{u}}= {\langle {\boldsymbol{u}}\rangle} + {\boldsymbol{u}}' = {\boldsymbol{U}}+ {\boldsymbol{u}}'.$$
Here we defined $U \equiv {\langle {\boldsymbol{u}}\rangle}$. We assume
that the average (or filtering) operator is such that
${\langle {\boldsymbol{u}}' \rangle} = 0$ and that it commutes with time
and space derivatives.

### Equation for the averaged velocity

To obtain the equation for the averaged velocity ${\boldsymbol{U}}$, we
take the average of the Navier-Stokes equation

$${\langle {\text{D}_t}{\boldsymbol{u}}\rangle} = {\partial}_t {\boldsymbol{U}}+ {\langle {\boldsymbol{u}}\cdot {\boldsymbol{\nabla}}{\boldsymbol{u}}\rangle} = - {\boldsymbol{\nabla}}P
+ \nu {\boldsymbol{\nabla}}^2 {\boldsymbol{U}}.$$

The nonlinear term is equal to
${\langle {\boldsymbol{u}}\cdot {\boldsymbol{\nabla}}{\boldsymbol{u}}\rangle} = {\boldsymbol{U}}\cdot {\boldsymbol{\nabla}}{\boldsymbol{U}}+ {\boldsymbol{\nabla}}\cdot
{\langle {\boldsymbol{u}}' {\boldsymbol{u}}' \rangle}$. The second term
is the divergence of the Reynolds tensor
${\langle {\boldsymbol{u}}' {\boldsymbol{u}}' \rangle}$.

Finally, we obtain

$${\partial}_t {\boldsymbol{U}}+  {\boldsymbol{U}}\cdot {\boldsymbol{\nabla}}{\boldsymbol{U}}= - {\boldsymbol{\nabla}}P
+ \nu {\boldsymbol{\nabla}}^2 {\boldsymbol{U}}- {\boldsymbol{\nabla}}\cdot {\langle {\boldsymbol{u}}' {\boldsymbol{u}}' \rangle}.$$

This equation is very similar to the Navier-Stokes equation except that
there is a additional term implying the Reynolds tensor.

### Equation for the Reynolds tensor

Do it: derive the equation for the Reynolds tensor
${\langle u_i' u_j' \rangle}$. Show that it involves third order moment
of the velocity fluctuations!

We want to compute the time derivative of ${\langle u_i' u_j' \rangle}$.
Let’s start with the equations

$${\partial}_t u_i + u_k {\partial}_k u_i = -{\partial}_i p + \nu {\boldsymbol{\nabla}}^2 u_i,$$
and
$${\partial}_t U_i + U_k {\partial}_k U_i = -{\partial}_i P + \nu {\boldsymbol{\nabla}}^2 U_i
- {\partial}_k {\langle u_k' u_i' \rangle}.$$

### A problem: closing the system of equations

We could theoretically obtain an infinity of equations for different
moments of the velocity fluctuations ${\langle u_i' u_j' \rangle}$,
${\langle u_i' u_j' u_k' \rangle}$,
${\langle u_i' u_j' u_k' u_l' \rangle}$...

In practice, such infinite set of equations is not tractable so we have
to close this system at one degree of approximation. For example, one
can express the divergence of the Reynolds tensor as a function of the
average velocity.

### A first solution: turbulent diffusion $\nu_t$ and mixing scale

A first “naive” idea is to use a turbulent diffusion. A strong effect of
turbulence is to increase mixing, similarly as diffusion.

So we can formally write the divergence of the Reynolds tensor as a
diffusive term
$- {\boldsymbol{\nabla}}\cdot {\langle {\boldsymbol{u}}' {\boldsymbol{u}}' \rangle} = \nu_t{\boldsymbol{\nabla}}^2 {\boldsymbol{U}}$,
where $\nu_t({\boldsymbol{x}}, t)$ is the turbulent viscosity. To really
close the equation, we would have to express the turbulent viscosity as
a function of ${\boldsymbol{U}}$. By dimensional analysis, we see that
we need a field of characteristic length scale $a({\boldsymbol{x}})$.

Do it: find the “new” Navier-Stokes equations for the average velocity
${\boldsymbol{U}}$ as a function of $a({\boldsymbol{x}})$.

Methods
-------

### Experiments

Important measurement methods.

-   One point measurement: hot wire, Pitot tube, ...

-   PIV,

-   particule tracking.

Taylor approximation, also know as the “frozen turbulence” hypothesis.
$u(t) \rightarrow u(x)$

### Simulations (DNS, RANS, LES)

Three main types of flow simulations:

-   Direct Numerical Simulations (DNS), Navier-Stokes equations without
    approximation. The most demanding in terms of computational
    resources.

-   Large Eddy Simulations (LES), Space-average Navier-Stokes equations,

-   Reynolds-Average Navier-Stokes simulations (RANS), Time-average
    Navier-Stokes equations.

LES and RANS need turbulence models.

Example of the self-similar turbulent jet
-----------------------------------------

### Weakly non parallel flow scaling laws

Figure \[fig\_weakly\_non\_parallel\_flows\] shows examples of weakly
non parallel flows.

![Different weakly non parallel flows. From top to bottom: jet, mixing
layer, wake and flat boundary
layer.[]{data-label="fig_weakly_non_parallel_flows"}](../Figures/weakly_non_parallel_flows){width="0.8\linewidth"}

Streamwise component of the Navier-Stokes equation:

$${\partial}_x {\langle u \rangle}^2 + {\partial}_x {\langle u'^2 \rangle} + {\partial}_y {\langle v \rangle}{\langle u \rangle} +
{\partial}_y {\langle v'u' \rangle} =
- {\partial}_x p + \nu {\boldsymbol{\nabla}}^2 {\langle u \rangle}$$

Neglect terms: weakly non parallel flow and large $Re$:
$${\partial}_x {\langle u \rangle}^2 + {\partial}_y {\langle v \rangle}{\langle u \rangle} +
{\partial}_y {\langle v'u' \rangle} = 0$$

### Flux of Conserved quantities in the jet

Cylindrical coordinate

Flux of mass (here volume)
$$\mathcal{M} = 2\pi \int_0^\infty {\langle u_z \rangle} r dr.$$

Flux of momentum
$$\mathcal{P} = 2\pi \int_0^\infty {\langle {u_z}^2 \rangle} r dr.$$

Flux of kinetic energy
$$\mathcal{E} = 2\pi \int_0^\infty {\langle u_z {{\boldsymbol{u}}}^2/2 \rangle} r dr.$$

The flux of momentum is constant along the streamwise direction ($z$).

### Self-similarity, decay law, prediction

Hypothesis (observed in experimental data): $$\begin{aligned}
{\langle u_z \rangle} &= U(z) F(\xi), \\
{\langle u_r \rangle} &= U(z) G(\xi),\end{aligned}$$ where
$\xi = r/b(z)$ and $b(z)$ is a characteristic width of the jet.

This hypothesis is related to a symmetry of the Euler equation.

#### Consequence of the self-similarity + divergence free flow:

 \
$${\boldsymbol{\nabla}}\cdot {\langle {\boldsymbol{u}}\rangle} \Rightarrow
\frac{1}{r} {\partial}_r (r {\langle u_r \rangle}) + {\partial}_z {\langle u_z \rangle} = 0.$$

Change of variables $(z, r) \rightarrow (\tilde z=z, \xi=r/b(z))$

$$\frac{1}{\xi} d_\xi (\xi G) + \frac{bU'}{U} F(\xi)
- b' \xi d_\xi F = 0.$$

Theorem $\Rightarrow$ $b'$ and $z d\log U / dz$ are 2 constants. This
implies that $b(z) = \alpha z$ and $U \propto z^{-1}$.

### Consequences on fluxes of conserved quantities

#### Momentum

 \
With self-similarity, we show that

$$\mathcal{P} = \lambda_2 b^2 U^2,$$ where $\lambda_2$ is a constant.

Since $\mathcal{P}$ is conserved, we find that
$U \propto 1/b \propto 1/x$.

#### Mass: entrainment of ambient fluid

 \
$$\mathcal{M} = \lambda_1 b^2 U \propto x.$$

The mass flux increases.

#### Kinetic energy: dissipation

 \
The flux of kinetic energy
$$\mathcal{E} = \lambda_3 b^2 U^3 \propto 1/x$$ decreases because of
dissipation of energy due to viscosity.

### Turbulent viscosity to close the equations

Statistical descriptions
------------------------

### Fourier transforms

We define the continuous Fourier transform on a periodic domain $[0, L]$
as: $$\hat u(k) = \int_0^L u(x) e^{-ikx} \frac{dx}{L},$$ where
$k = 2\pi / \lambda$. The inverse Fourier transform is:
$$u(x) = \sum \hat u(k) e^{-ikx}.$$

### Spectra

Spectra are defined by equations like this one:
$${\langle {\boldsymbol{u}}^2/2 \rangle} = \int_0^\infty dk E(k)$$

Richardson cascade

$U^3/L \sim u(l)^3/l \sim {\varepsilon}$ is constant in the inertial
range (for scales $l$ between the injection scale and the dissipation
scale).

This gives the scaling law for the velocity at scale $l$:
$$u(l) \propto ({\varepsilon}l)^{1/3}$$

In terms of spectra, this gives ($k \sim 1/l$):

$$E(k) \propto u(l)^2 k^{-1} \propto {\varepsilon}^{2/3} k^{-5/3}.$$

Kolmogorov spectrum.
