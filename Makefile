
SHELL := /bin/bash

name := courseM1_PA_instabilities_turbulence
fullname := Latex/$(name)
figures := tmp/fig_pitchfork_bifurcation.png tmp/fig_supersub.png

LATEX := pdflatex -shell-escape -synctex=1
LATEXMK := latexmk -pdf -pdflatex="$(LATEX)"

.PHONY: all clean cleanall latex pdf html

html: book/tmp/fig_pitchfork_bifurcation.png book/tmp/fig_supersub.png
	time pdm run build

pdf-book: book/tmp/fig_pitchfork_bifurcation.png book/tmp/fig_supersub.png
	time pdm run build-pdf

cleanweb:
	rm -rf book/_build

format:
	mdformat README.md book --wrap 82
	black book

pdf: $(name).pdf

clean:
	cd Latex && \
	rm -f $(name).log $(name).aux $(name).out $(name).bbl $(name).blg \
	$(name).tmp

cleanall: clean
	rm -rf tmp
	rm -f $(fullname).pdf

latex:
	cd Latex && $(LATEXMK) $(name).tex

$(name).pdf: $(fullname).tex $(figures)
	cd Latex && $(LATEXMK) $(name).tex && cp $(name).pdf ..

tmp/fig_pitchfork_bifurcation.png: Pyfig/make_fig_pitchfork_bifurcation.py
	python Pyfig/make_fig_pitchfork_bifurcation.py save_for_tex

tmp/fig_supersub.png: Pyfig/make_fig_supersub.py
	python Pyfig/make_fig_supersub.py save_for_tex

book/tmp/fig_pitchfork_bifurcation.png: tmp/fig_pitchfork_bifurcation.png
	mkdir -p book/tmp
	cp tmp/fig_pitchfork_bifurcation.png book/tmp

book/tmp/fig_supersub.png: tmp/fig_supersub.png
	mkdir -p book/tmp
	cp tmp/fig_supersub.png book/tmp
