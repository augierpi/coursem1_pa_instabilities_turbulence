from util import np, plt, save

n = 100

fig = plt.figure()
ax = fig.add_axes([0.15, 0.14, 0.8, 0.8])

xs = np.linspace(-2, 0, n)
ax.plot(xs, np.zeros_like(xs), "-b")

xs = np.linspace(0, 2, n)
ax.plot(xs, np.zeros_like(xs), "--b")

yfs = np.sqrt(xs) / 10
ax.plot(xs, yfs, "r-")
ax.plot(xs, -yfs, "r-")

ax.set_xlabel(r"$\tilde\Omega$")
ax.set_ylabel(r"$\theta'$")

save(fig, "../tmp/fig_pitchfork_bifurcation.png")
