import os
import sys

import numpy as np
import matplotlib.pyplot as plt

path_tmp = os.path.join(os.path.split(os.path.split(__file__)[0])[0], "tmp")
if not os.path.exists(path_tmp):
    os.makedirs(path_tmp)

if "save_for_tex" not in sys.argv:
    plt.ion()


def save(fig, name):
    if "save_for_tex" in sys.argv:
        fig.savefig(os.path.join(path_tmp, name))
    else:
        plt.show
