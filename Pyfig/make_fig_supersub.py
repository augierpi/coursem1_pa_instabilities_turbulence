from util import np, plt, save

n = 100

fig = plt.figure(figsize=[9, 5])
ax = fig.add_axes([0.1, 0.14, 0.38, 0.8])

xs = np.linspace(-0.2, 0, n)
ax.plot(xs, np.zeros_like(xs), "-b")

xs = np.linspace(0, 0.2, n)
ax.plot(xs, np.zeros_like(xs), "--b")

xs = np.linspace(0.0, 0.2, n)
yfs = np.sqrt(0.5 * (-1 + np.sqrt(1 + 4 * xs)))
ax.plot(xs, yfs, "r-")
ax.plot(xs, -yfs, "r-")

ax.set_xlabel(r"$C1$")
ax.set_ylabel(r"$X$")

ax.set_title("supercritical")


ax = fig.add_axes([0.6, 0.14, 0.38, 0.8])

xs = np.linspace(-0.32, 0, n)
ax.plot(xs, np.zeros_like(xs), "-b")

xs = np.linspace(0, 0.1, n)
ax.plot(xs, np.zeros_like(xs), "--b")

xs = np.linspace(-0.25, 0.1, n)
yfs = np.sqrt(0.5 * (1 + np.sqrt(1 + 4 * xs)))
ax.plot(xs, yfs, "r-")
ax.plot(xs, -yfs, "r-")

xs = np.linspace(-0.25, 0.0, n)
yfs = np.sqrt(0.5 * (1 - np.sqrt(1 + 4 * xs)))
ax.plot(xs, yfs, "r--")
ax.plot(xs, -yfs, "r--")

ax.set_xlabel(r"$C1$")
ax.set_ylabel(r"$X$")
ax.set_title("subcritical")

save(fig, "../tmp/fig_supersub.png")
