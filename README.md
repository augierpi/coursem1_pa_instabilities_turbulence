# Course M1 Instabilities and turbulence

This repository contains the source of a pdf and
[a website](https://augierpi.gricad-pages.univ-grenoble-alpes.fr/coursem1_pa_instabilities_turbulence)
used during the course.

## Dependencies

We will use the following programs: LaTeX (pdflatex, latexmk), make, Python and
Fluidsim. We describe how to install and setup all these tools in the file
[install.md](https://gricad-gitlab.univ-grenoble-alpes.fr/augierpi/coursem1_pa_instabilities_turbulence/-/blob/master/install.md).

## Download and update the source

Once the program [mercurial](http://mercurial.selenic.com/) is installed and
correctly setup (as explained in the
[install.md](https://gricad-gitlab.univ-grenoble-alpes.fr/augierpi/coursem1_pa_instabilities_turbulence/-/blob/master/install.md)
file), one can clone the repo with:

```bash
hg clone https://gricad-gitlab.univ-grenoble-alpes.fr/augierpi/coursem1_pa_instabilities_turbulence.git
```

Later, if you need to update this repository to the last version, run:

```bash
cd coursem1_pa_instabilities_turbulence
hg pull -u
```

## Build the pdf of the course

Everything is compiled with the program make (just run `make`, see the Makefile).

If it doesn't work, you can open an issue
[here](https://gricad-gitlab.univ-grenoble-alpes.fr/augierpi/coursem1_pa_instabilities_turbulence/-/issues).
Explain what you tried and what you obtain (copy/paste the error log).

## Your report in your repository

See the
[dedicated page](https://gricad-gitlab.univ-grenoble-alpes.fr/augierpi/coursem1_pa_instabilities_turbulence/-/issues/12)
and the
[LaTeX template](https://gricad-gitlab.univ-grenoble-alpes.fr/augierpi/report_research_project_template).

## For writers

Install the requirements for building the "book" with

```
pip install -r book/requirements.txt
```

or with `pdm`:

```sh
pdm install --dev --no-self
```

mdformat and black are used to format the sources of the book (command
`make format`). These packages can be installed for example with `pipx`.

```
pip install pipx
pipx install mdformat-myst --include-deps
pipx install black
```
