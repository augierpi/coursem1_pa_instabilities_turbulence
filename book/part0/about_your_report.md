# Practical task: write a report in Latex and put the source in a repository

You will have to:

- Make a report written in LaTeX with 2 parts:

  1. your own study with your own figures and tables on what we've learned on
     simple dynamical system (we will work on that!),
  1. A presentation on another subject of your choice on fluid instabilities
     and/or turbulence.

- Work on your own repository on https://gricad-gitlab.univ-grenoble-alpes.fr/
  containing the source of the report.

You will be marked on the result of the following commands so this has to work and
produce the pdf `report.pdf` (Check that it works!):

```bash
hg clone https://gricad-gitlab.univ-grenoble-alpes.fr/yourusername/report_research_project_template.git
cd report_research_project_template
make cleanall
make
evince report.pdf &
```

Of course, replace yourusername by your UGA username.

Don't worry, we'll learn everything you need to do that and to understand how it
works!

A first step is to fork the project
https://gricad-gitlab.univ-grenoble-alpes.fr/augierpi/report_research_project_template.

- Login on https://gricad-gitlab.univ-grenoble-alpes.fr
- Go to
  https://gricad-gitlab.univ-grenoble-alpes.fr/augierpi/report_research_project_template
- Click on the Fork button!

When it's done, please send a message in
[this issue](https://gricad-gitlab.univ-grenoble-alpes.fr/augierpi/coursem1_pa_instabilities_turbulence/-/issues/19)
with the URL of your repository (so that I can (i) see who has managed to fork the
project and (ii) know your username on gricad-gitlab).
