# Example of a script to save a simple figure

```{important}
It is a good practice to produce figures fully programmatically and it is much simpler to have one script per figure.
```

Here is a very simple script which can save a simple figure produced with
Matplotlib:

```{eval-rst}
.. literalinclude:: save_simple_fig.py
```
