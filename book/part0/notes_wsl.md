# Notes on setting up WSL (Windows Subsystem for Linux)

You can now install Ubuntu inside Windows. It is called the
[Windows Subsystem for Linux (WSL)](https://ubuntu.com/wsl).

There are many tutorials on the web explaining how to install WSL. The first thing
to do is always to update Windows! I would advice
[this tutorial](https://ubuntu.com/tutorials/install-ubuntu-on-wsl2-on-windows-11-with-gui-support).
For the time being (Sept. 2022), I guess Ubuntu 20.04 is the better option.

Here, I summarize what has to be done after the WSL installation before the main
installation procedure described in the file [](install.md).

See also
[this article about how to access Ubuntu files from Windows](https://www.howtogeek.com/426749/how-to-access-your-linux-wsl-files-in-windows-10/).

```{admonition} Turn on "Use Ctrl+Shift+C/V as Copy/Paste"

For a seamless experience, I suggest turning on the "Use Ctrl+Shift+C/V as
Copy/Paste" option. Do this by right clicking on the Ubuntu app title bar, and
clicking on Properties.

```
