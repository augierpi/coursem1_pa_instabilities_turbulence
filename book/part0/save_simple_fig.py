#!/usr/bin/env python3
"""
Example of script to save a simple figure

"""

# First the imports from the standard library:
from pathlib import Path
import sys

# Then the imports from standard packages:
import numpy as np
import matplotlib.pyplot as plt

# path of the directory where is this file
here = Path(__file__).absolute().parent
# path of the directory where the figure will be saved
path_dir_save = here / "../tmp/"
path_dir_save.mkdir(exist_ok=True)

# We produce (compute or load) the data to be plotted:
tmin = 0.0  # s
tmax = 5.0  # s
nb_points = 50

times = np.linspace(tmin, tmax, nb_points)

z0 = 100.0  # m
v0 = 1.0  # m/s
g = 9.8  # m/s**2

vs = v0 - g * times
zs = z0 + vs * times

# We make the figure and save it:
fig, (ax0, ax1) = plt.subplots(2, figsize=(6, 4))

ax0.plot(times, vs)
ax0.set_xlabel("$t$")
ax0.set_ylabel("$v$")

ax1.plot(times, zs)
ax1.set_xlabel("$t$")
ax1.set_ylabel("$z$")

fig.tight_layout()

if "SAVE" in sys.argv:
    fig.savefig(path_dir_save / "fig_simple.png")
else:
    plt.show()
