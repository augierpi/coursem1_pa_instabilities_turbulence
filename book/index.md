# Course Instabilities and Turbulence

This is the new website of a Master 1 course on instabilities and turbulence.

```{danger}
This site is a work in progess. Many things should be improved!
```

```{admonition} On the author
I'm [Pierre
Augier](http://www.legi.grenoble-inp.fr/people/Pierre.Augier/index.html), a
researcher at [LEGI](http://www.legi.grenoble-inp.fr/) studying geophysical
turbulence with experiments and numerical simulations. I'm the maintainer of
the [FluidDyn project](https://fluiddyn.readthedocs.io). I did a PhD at LadHyx
(Paris) and 2 post-docs at KTH (Stockholm) and DAMTP (Cambridge, UK).
```

```{note}
This course is inspired by many books, courses and documents. In particular, I
used a lot the course of Olivier Cadot `Introduction à la turbulence` given
at ENSTA and the book `Hydrodynamique physique` {cite:ps}`GuyonHulinPetit2021`.
```

- This course is as simple and gradual as possible. You should be able to
  understand everything. Do not hesitate to ask questions!

- This course has to be interactive! YOU will have to work. With pen and paper but
  also with computers. We will use tools really used in sciences and research: in
  particular Linux, [Mercurial](https://www.mercurial-scm.org), LaTeX and
  Python...

- Practical work with Linux. You will need to work with a computer with Ubuntu
  `>= 18.04`. We will use UGA computers but you can also install Ubuntu on your
  computer.

## Practical sessions

Part of this course will be dedicated to work on a practical task: you will have
to write a report in Latex (with figures) and to put the source of this report in
a repository on the website <https://gricad-gitlab.univ-grenoble-alpes.fr>.
Instructions and advice about the content of your report are described in
[this page](part0/about_your_report.md). We will spend some sessions for you to
learn enough to be able to do this task.

- [Introduction Linux terminal and Bash](part0/intro_bash.ipynb)

- [Introduction Gitlab and Mercurial](part0/Gitlab_with_Mercurial.ipynb) (basics)

- [Practical work: install the tools](part0/install.md). Note that you can
  copy/paste commands!

- Practical work: clone this repository

- Practical work:
  [Fork the report template and clone your fork locally](part0/about_your_report.md)

- Introduction to LaTeX by looking at the code of the report template

- [A bit more on Mercurial](part0/Gitlab_with_Mercurial.ipynb) to be able to push
  your changes into your fork on Gitlab.

- Practical work: improve your LaTeX report and update your repository.

- Practical work: run small simulations and produce simple figures (see the
  example [](part0/example_script_save_simple_fig.md) and the directory
  [Pysimul](https://gricad-gitlab.univ-grenoble-alpes.fr/augierpi/coursem1_pa_instabilities_turbulence/-/tree/master/Pysimul)).

  You can copy/paste this directory into your repository. Then, try the scripts
  and create new scripts to save figures that show what you understood on simple
  dynamical systems during this course. Finally, include these figures in your
  repository and in your tex file.

## Clone this repository

Clone the repository with Mercurial (and the extension hg-git, as explained
[here](https://fluiddyn.readthedocs.io/en/latest/mercurial_heptapod.html)):

```
hg clone https://gricad-gitlab.univ-grenoble-alpes.fr/augierpi/coursem1_pa_instabilities_turbulence.git
```

or with ssh (so you need to create a ssh key and copy the public key on
https://gricad-gitlab.univ-grenoble-alpes.fr):

```
hg clone git@gricad-gitlab.univ-grenoble-alpes.fr:augierpi/coursem1_pa_instabilities_turbulence.git
```

## References

```{bibliography}
```
