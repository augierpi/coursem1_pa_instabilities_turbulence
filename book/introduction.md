# Instabilities and turbulence

## Common phenomena and important concepts

**Instability** is a very general concept: "when a state is unstable". So it is
not surprising that instabilities are everywhere: in flows (what we will study
during this course) but also in many other systems. Here are two examples from the
fields of solid mechanics and fluid-structure interactions:

- Buckling (for example of columns, see a
  [video at $t = 40$ s](https://vimeo.com/132312421)

- Suspension bridge flutter and fluid-structure interactions
  [see this video](https://youtu.be/j-zczJXSxnw)..

A system can be characterized as "**turbulent**" when structures with a **broad
range of scales interact non-linearly** in a system with **several degrees of
freedom**.

Instabilities and turbulence are very general concepts that can be applied to a
lot of systems (populations, ecology, society, climate, etc.).

Flow instabilities and turbulence are subjects with many practical interests
(vehicles, machines, biological fluids, geophysical fluids, etc.). They have been
widely studied. They are very good models for research on instabilities and
turbulence in general.

````{subfigure} AB

```{image} ./figs/2013_FordFusion_CFDTopandSide.png
:alt: 2013_FordFusion_CFDTopandSide.png
:width: 90%
```

```{image} ./figs/airfoil_turbulence.jpg
:alt: airfoil_turbulence.jpg
:width: 90%
```

Turbulent wake of vehicles, car and airfoil

````

```{admonition} Two types of descriptions:
Mechanisms (instability) and
statistics (turbulence).
```

## Reynolds experiment and Reynolds number

```{figure} ./figs/Reynolds_exp.jpg
---
width: 70%
name: fig_Reynolds_exp
---
Sketch of Reynolds's dye experiment, taken from his 1883 paper.
```

[Osborne Reynolds (1842-1912)](https://en.wikipedia.org/wiki/Osborne_Reynolds)
carried out experiments on the flow in a long tube. A drawing of Reynolds and its
experimental setup is shown in {numref}`fig_Reynolds_exp`. This
[video of a recent experiment](https://www.youtube.com/watch?v=xiX5PfFxmIs) helps
to understand what appends.

Water flow through a tube and a small flow volume of ink is injected at the
beginning of the tube.

- For low velocity, the flow is laminar and the filament of ink stays linear along
  the tube.

- For larger velocity the flow becomes unstable and there is a transition to
  turbulence. The filament of ink is perturbed and there is mixing.

More precisely, depending on the velocity and tube radius, the flow can be:

- laminar (straight not perturbed ink filament),
- perturbed,
- turbulent (fast mixing).

This flow exhibits a hysteresis! The instability is said "**subcritical**".

When the tube is long enough, the length of the tube is not a relevant parameter
and there are only three relevant physical quantities:

- viscosity of the water $\nu$ (in m$^2$/s),
- diameter of the tube $D$ (in m),
- velocity of the water $U$ (in m/s).

We see that there are only 2 units and 3 variables so we can only form 1
non-dimensional number
[in application of the $\Pi$ theorem](https://en.wikipedia.org/wiki/Buckingham_%CF%80_theorem):

$$ Re = \frac{U D}{\nu}.$$

This is the Reynolds number. It is one of the most important non-dimensional
numbers in fluid dynamics.

## Navier-Stokes equations: diffusion and advection

```{math}
\newcommand{\p}{\partial}
\newcommand{\vv}{\boldsymbol{v}}
\newcommand{\xx}{\boldsymbol{x}}
\newcommand{\bnabla}{\boldsymbol{\nabla}}
\newcommand{\Dt}{\text{D}_t}
```

```{role} raw-latex(raw)
:format: latex
```

{raw-latex}`\newcommand{\p}{\partial}`
{raw-latex}`\newcommand{\vv}{\boldsymbol{v}}`
{raw-latex}`\newcommand{\xx}{\boldsymbol{x}}`
{raw-latex}`\newcommand{\bnabla}{\boldsymbol{\nabla}}`
{raw-latex}`\newcommand{\Dt}{\text{D}_t}`

The Navier-Stokes equations for an incompressible ($\bnabla\cdot \vv = 0$) fluid
can be written as

```{math}
\p_t \vv + \vv \cdot \bnabla \vv = -\bnabla p + \nu \bnabla^2 \vv,
```

where $\vv$ is the velocity, $p$ is the rescaled pressure (in m$^2$/s$^{2}$),
$\nu$ is the cinematic viscosity (in m$^2$/s) and $\bnabla^2$ is the Laplacian
operator.

```{important}
You have to know by heart these equations. To be able to write them
without thinking too much in vectorial form and to really understand this
notation (for example $\vv \cdot \bnabla \vv |_i = v_j \p_j v_i$). You have to
be able to define all quantities ($\vv$, $p$, $\nu$, $t$, $\bnabla$) and to know
their units.  You have to know and understand the physical meaning of all
terms.
```

### Diffusive transport

The last term of the NS equation is the same that the term appearing in the
temperature equation describing the heat diffusion:

```{math}
\p_t T = \kappa \bnabla^2 T.
```

The diffusive time is $\tau_\nu = L^2/\nu$.

### Advective transport

The Euler equation describes the dynamics of an incompressible perfect fluid
(without viscosity):

```{math}
\p_t \vv + \vv \cdot \bnabla \vv = -\bnabla p
```

The advective term is nearly the same in the equation of advection of a field
$f(x, t)$ by a constant velocity $U$:

```{math}
\p_t f(x,t) + U \p_x f(x, t) = 0.
```

Notation for the advective term: $\p_t \vv + \vv \cdot \bnabla \vv = \Dt \vv$.

```{admonition} Remark on vocabulary
Advection and convection.
```

The advective time is $\tau_a = L/U$.

### Scaling analysis of the Navier-Stokes equation

First step: characteristic values for the velocity $U$, the length scale $L$ and
the pressure $P$. We can define nondimensional variables as:

```{math}
\vv' = \vv / U, \quad \xx' = \xx / L, \quad
t' = t / (L/U) \text{ and } p' = p / P.
```

Let's compute the scaling of the different terms of the incompressible
Navier-Stokes equations. After some equations (white board), we get the scaling
for the pressure: $P \sim U^2$.

We obtain the nondimensional incompressible Navier-Stokes equations:

```{math}
\p_{t'} \vv' + \vv' \cdot \bnabla' \vv' = -\bnabla' p' + \frac{1}{Re} \bnabla'^2 \vv',
```

with $Re = UL/\nu$.

The Reynolds number can be written as a ratio of forces:

```{math}
Re = \frac{|\vv \cdot \bnabla \vv|}{|\nu \bnabla^2 \vv|}.
```

or of time scales:

```{math}
Re = \frac{UL}{\nu} = \frac{\frac{L^2}{\nu}}{\frac{L}{U}} =
\frac{\tau_\nu}{\tau_a}.
```

## An example: the wake behind a cylinder

{numref}`fig_wake_cylinder_regimes` shows four pictures of the flow behind a
cylinder. We see that these four states are qualitatively very different, from
laminar (regular, steady, symmetrical) to nearly turbulent.
{numref}`fig_wake_cylinder_turb` presents a really turbulent case.
[This video](https://www.youtube.com/watch?v=JI0M1gVNhbw) (at $t = 3$ min) shows a
clear example of the vortex shedding behind a cylinder for different velocities
(Reynolds numbers).

Let's recall and introduce very important concepts:

- For this flow, the control parameter is also the Reynolds number $Re$.

- Instabilities. Stable and unstable states.

- Breaking of symmetry (space and time).

- Vortex shedding with a characteristic frequency: Strouhal number
  $St = fD / U \simeq 0.2$...

- Transition to turbulence (see {numref}`fig_wake_cylinder_turb`).

- For this configuration there is no hysteresis. This is a "**supercritical**"
  instability.

```{figure} ./figs/wake_cylinder_regimes.jpg
---
name: fig_wake_cylinder_regimes
width: 75%
---
Visualization of a flow behind a cylinder for different Reynolds
numbers. `(a)` $Re = 1.54$; `(b)` $Re = 26$; `(c)` $Re = 200$; `(d)` $Re = 8000$. Taken
from GHP.
```

```{figure} ./figs/turb_wake_cylinder.jpg
---
name: fig_wake_cylinder_turb
width: 60%
---
Visualization of the turbulent wake of a cylinder at large Reynolds
number.
```

```{note}

Fluid-structure interaction... See [this video](https://www.youtube.com/watch?v=_Hbbkd2d3H8).

```

## Questions

- Write the vectorial version of the Navier-Stokes equations for an incompressible
  fluid.

- Give a definition of the Reynolds number as a ratio of two terms of the
  Navier-Stokes equations.

- Give the characteristic time of a diffusive process along a length $L$.

- Give the characteristic time of a advective process along a length $L$.

- What is the Strouhal number? What is its typical value for the wake of a
  cylinder?
