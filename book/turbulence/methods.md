# Methods

## Experiments

Important measurement methods.

- One point measurement: hot wire, Pitot tube, ...

- PIV,

- particule tracking.

Taylor approximation, also know as the “frozen turbulence” hypothesis.
$u(t) \rightarrow u(x)$

## Simulations (DNS, RANS, LES)

Three main types of flow simulations:

- Direct Numerical Simulations (DNS), Navier-Stokes equations without
  approximation. The most demanding in terms of computational resources.

- Large Eddy Simulations (LES), Space-average Navier-Stokes equations,

- Reynolds-Average Navier-Stokes simulations (RANS), Time-average Navier-Stokes
  equations.

LES and RANS need turbulence models.
