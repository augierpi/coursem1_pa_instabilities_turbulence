# Average and Reynolds decomposition

## Decomposition of the flow field

We decompose the flow field using a averaging (or filtering) operator
${\langle . \rangle}$ as:

$${\boldsymbol{u}}= {\langle {\boldsymbol{u}}\rangle} + {\boldsymbol{u}}' = {\boldsymbol{U}}+ {\boldsymbol{u}}'.$$

Here we defined $U \equiv {\langle {\boldsymbol{u}}\rangle}$. We assume that the
average (or filtering) operator is such that
${\langle {\boldsymbol{u}}' \rangle} = 0$ and that it commutes with time and space
derivatives.

## Equation for the averaged velocity

To obtain the equation for the averaged velocity ${\boldsymbol{U}}$, we take the
average of the Navier-Stokes equation

$${\langle {\text{D}_t}{\boldsymbol{u}}\rangle} = {\partial}_t {\boldsymbol{U}}+ {\langle {\boldsymbol{u}}\cdot {\boldsymbol{\nabla}}{\boldsymbol{u}}\rangle} = - {\boldsymbol{\nabla}}P
+ \nu {\boldsymbol{\nabla}}^2 {\boldsymbol{U}}.$$

The nonlinear term is equal to
${\langle {\boldsymbol{u}}\cdot {\boldsymbol{\nabla}}{\boldsymbol{u}}\rangle} = {\boldsymbol{U}}\cdot {\boldsymbol{\nabla}}{\boldsymbol{U}}+ {\boldsymbol{\nabla}}\cdot
{\langle {\boldsymbol{u}}' {\boldsymbol{u}}' \rangle}$.
The second term is the divergence of the Reynolds tensor
${\langle {\boldsymbol{u}}' {\boldsymbol{u}}' \rangle}$.

Finally, we obtain

$${\partial}_t {\boldsymbol{U}}+  {\boldsymbol{U}}\cdot {\boldsymbol{\nabla}}{\boldsymbol{U}}= - {\boldsymbol{\nabla}}P
+ \nu {\boldsymbol{\nabla}}^2 {\boldsymbol{U}}- {\boldsymbol{\nabla}}\cdot {\langle {\boldsymbol{u}}' {\boldsymbol{u}}' \rangle}.$$

This equation is very similar to the Navier-Stokes equation except that there is a
additional term implying the Reynolds tensor.

## Equation for the Reynolds tensor

Do it: derive the equation for the Reynolds tensor ${\langle u_i' u_j' \rangle}$.
Show that it involves third order moment of the velocity fluctuations!

We want to compute the time derivative of ${\langle u_i' u_j' \rangle}$. Let’s
start with the equations

$${\partial}_t u_i + u_k {\partial}_k u_i = -{\partial}_i p + \nu {\boldsymbol{\nabla}}^2 u_i,$$

and

$${\partial}_t U_i + U_k {\partial}_k U_i = -{\partial}_i P + \nu {\boldsymbol{\nabla}}^2 U_i
- {\partial}_k {\langle u_k' u_i' \rangle}.$$

## A problem: closing the system of equations

We could theoretically obtain an infinity of equations for different moments of
the velocity fluctuations ${\langle u_i' u_j' \rangle}$,
${\langle u_i' u_j' u_k' \rangle}$, ${\langle u_i' u_j' u_k' u_l' \rangle}$...

In practice, such infinite set of equations is not tractable so we have to close
this system at one degree of approximation. For example, one can express the
divergence of the Reynolds tensor as a function of the average velocity.

## A first solution: turbulent diffusion $\nu_t$ and mixing scale

A first “naive” idea is to use a turbulent diffusion. A strong effect of
turbulence is to increase mixing, similarly as diffusion.

So we can formally write the divergence of the Reynolds tensor as a diffusive term
$- {\boldsymbol{\nabla}}\cdot {\langle {\boldsymbol{u}}' {\boldsymbol{u}}' \rangle} = \nu_t{\boldsymbol{\nabla}}^2 {\boldsymbol{U}}$,
where $\nu_t({\boldsymbol{x}}, t)$ is the turbulent viscosity. To really close the
equation, we would have to express the turbulent viscosity as a function of
${\boldsymbol{U}}$. By dimensional analysis, we see that we need a field of
characteristic length scale $a({\boldsymbol{x}})$.

Do it: find the “new” Navier-Stokes equations for the average velocity
${\boldsymbol{U}}$ as a function of $a({\boldsymbol{x}})$.
