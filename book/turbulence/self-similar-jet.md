# Bonus: example of the self-similar turbulent jet

## Weakly non parallel flow scaling laws

{numref}`fig_weakly_non_parallel_flows` shows examples of weakly non parallel
flows.

```{figure} ../figs/weakly_non_parallel_flows.jpg
---
width: 80%
name: fig_weakly_non_parallel_flows
---
Different weakly non parallel flows. From top to bottom: jet, mixing
layer, wake and flat boundary
layer.
```

Streamwise component of the Navier-Stokes equation:

$${\partial}_x {\langle u \rangle}^2 + {\partial}_x {\langle u'^2 \rangle} + {\partial}_y {\langle v \rangle}{\langle u \rangle} +
{\partial}_y {\langle v'u' \rangle} =
- {\partial}_x p + \nu {\boldsymbol{\nabla}}^2 {\langle u \rangle}$$

Neglect terms: weakly non parallel flow and large $Re$:

$${\partial}_x {\langle u \rangle}^2 + {\partial}_y {\langle v \rangle}{\langle u \rangle} +
{\partial}_y {\langle v'u' \rangle} = 0$$

## Flux of Conserved quantities in the jet

Cylindrical coordinate

Flux of mass (here volume)

$$\mathcal{M} = 2\pi \int_0^\infty {\langle u_z \rangle} r dr.$$

Flux of momentum

$$\mathcal{P} = 2\pi \int_0^\infty {\langle {u_z}^2 \rangle} r dr.$$

Flux of kinetic energy

$$\mathcal{E} = 2\pi \int_0^\infty {\langle u_z {{\boldsymbol{u}}}^2/2 \rangle} r dr.$$

The flux of momentum is constant along the streamwise direction ($z$).

## Self-similarity, decay law, prediction

Hypothesis (observed in experimental data):

$$\begin{aligned}
{\langle u_z \rangle} &= U(z) F(\xi), \\
{\langle u_r \rangle} &= U(z) G(\xi),\end{aligned}$$

where $\xi = r/b(z)$ and $b(z)$ is a characteristic width of the jet.

This hypothesis is related to a symmetry of the Euler equation.

### Consequence of the self-similarity + divergence free flow:

$${\boldsymbol{\nabla}}\cdot {\langle {\boldsymbol{u}}\rangle} \Rightarrow
\frac{1}{r} {\partial}_r (r {\langle u_r \rangle}) + {\partial}_z {\langle u_z \rangle} = 0.$$

Change of variables $(z, r) \rightarrow (\tilde z=z, \xi=r/b(z))$

$$\frac{1}{\xi} d_\xi (\xi G) + \frac{bU'}{U} F(\xi) - b' \xi d_\xi F = 0.$$

Theorem $\Rightarrow$ $b'$ and $z d\log U / dz$ are 2 constants. This implies that
$b(z) = \alpha z$ and $U \propto z^{-1}$.

## Consequences on fluxes of conserved quantities

### Momentum

With self-similarity, we show that

$$\mathcal{P} = \lambda_2 b^2 U^2,$$

where $\lambda_2$ is a constant.

Since $\mathcal{P}$ is conserved, we find that $U \propto 1/b \propto 1/x$.

### Mass: entrainment of ambient fluid

$$\mathcal{M} = \lambda_1 b^2 U \propto x.$$

The mass flux increases.

### Kinetic energy: dissipation

The flux of kinetic energy

$$\mathcal{E} = \lambda_3 b^2 U^3 \propto 1/x$$

decreases because of dissipation of energy due to viscosity.

## Turbulent viscosity to close the equations
