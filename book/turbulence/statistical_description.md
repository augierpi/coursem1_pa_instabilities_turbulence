# Statistical descriptions

## Fourier transforms

We define the continuous Fourier transform on a periodic domain $[0, L]$ as:

$$\hat u(k) = \int_0^L u(x) e^{-ikx} \frac{dx}{L},$$

where $k = 2\pi / \lambda$. The inverse Fourier transform is:

$$u(x) = \sum \hat u(k) e^{-ikx}.$$

## Spectra

Spectra are defined by equations like this one:

$${\langle {\boldsymbol{u}}^2/2 \rangle} = \int_0^\infty dk E(k)$$

Richardson cascade

$U^3/L \sim u(l)^3/l \sim {\varepsilon}$ is constant in the inertial range (for
scales $l$ between the injection scale and the dissipation scale).

This gives the scaling law for the velocity at scale $l$:

$$u(l) \propto ({\varepsilon}l)^{1/3}$$

In terms of spectra, this gives ($k \sim 1/l$):

$$E(k) \propto u(l)^2 k^{-1} \propto {\varepsilon}^{2/3} k^{-5/3}.$$

Kolmogorov spectrum.
