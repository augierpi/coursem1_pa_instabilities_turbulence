# Effects of variable density

## Boussinesq approximation

For a compressible fluid, the conservation of mass can be written as:

$${\text{D}_t}{\rho_{\text{tot}}}+ {\rho_{\text{tot}}}{\boldsymbol{\nabla}}\cdot {\boldsymbol{u}}= 0 \Leftrightarrow
{\partial}_t {\rho_{\text{tot}}}+ {\boldsymbol{\nabla}}\cdot({\rho_{\text{tot}}}{\boldsymbol{u}}) = 0.$$

The Navier-Stokes equation is

$${\rho_{\text{tot}}}{\text{D}_t}{\boldsymbol{u}}= -{\boldsymbol{\nabla}}\tilde p + {\rho_{\text{tot}}}{\boldsymbol{g}}+ {\rho_{\text{tot}}}\nu {\boldsymbol{\nabla}}^2 {\boldsymbol{u}}.$$

The density is decomposed in 3 parts:
${\rho_{\text{tot}}}= \rho_0 +
\tilde\rho(z) + \rho'$, where the time average
density is $\bar\rho(z) = \rho_0 + \tilde\rho(z)$.

The Boussinesq approximation consists in replacing ${\rho_{\text{tot}}}$ by
$\rho_0$ everywhere except in the buoyancy term.

$${\text{D}_t}{\boldsymbol{u}}= -{\boldsymbol{\nabla}}\frac{\tilde p}{\rho_0}
  + \frac{{\rho_{\text{tot}}}{\boldsymbol{g}}}{\rho_0}
  + \nu {\boldsymbol{\nabla}}^2 {\boldsymbol{u}}.
$$

and the mass conservation becomes
${\boldsymbol{\nabla}}\cdot {\boldsymbol{u}}=
0$.

We can subtract to this equation its static solution obtained for
${\boldsymbol{u}}= 0$:

$$0 = -{\boldsymbol{\nabla}}\frac{\tilde p_s}{\rho_0} + \frac{\bar\rho {\boldsymbol{g}}}{\rho_0}$$

which gives

$${\text{D}_t}{\boldsymbol{u}}= -{\boldsymbol{\nabla}}p + {\boldsymbol{b}}+ \nu {\boldsymbol{\nabla}}^2 {\boldsymbol{u}},$$

where $p$ is the rescaled dynamic pressure and ${\boldsymbol{b}}$ is the buoyancy.

The conservation of internal energy and mass of salt can be rewritten as

$${\text{D}_t}{\rho_{\text{tot}}}= \kappa {\boldsymbol{\nabla}}^2 {\rho_{\text{tot}}},$$

which leads with some assumption to

$${\text{D}_t}b + N^2 u_z = \kappa {\boldsymbol{\nabla}}^2 b,$$

where $N = \sqrt{d_z \bar b_{\text{tot}}}$ is the Brunt-Väisälä frequency.

```{exercise}

Compute the frequency $\omega$ of a perturbation for a modified set of
equations for which you assume that there is no horizontal movement. Also
assume that the Brunt-Väisälä frequency is constant and that there is no
dissipation ($\nu = \kappa = 0$).

```

## Instabilities associated with density variations

### Unstable stratification, Rayleigh-Taylor instability

When a lighter fluid is placed below a denser fluid, the lighter fluid can go up
and the denser fluid go down. The picture of an experiment on such instability,
called Rayleigh-Taylor, is shown in {numref}`fig_rayleigh_taylor_DaviesWykes`.

```{figure} ../figs/rayleigh_taylor_DaviesWykes.png
---
width: 40%
name: fig_rayleigh_taylor_DaviesWykes
---
Picture of a Rayleigh-Taylor experiment. Taken from a
[video](https://youtu.be/NI85oC-3mJ0) produced by Davies Wykes and Dalziel
(DAMTP, University of Cambridge, UK).
```

### Rayleigh-Benard instability ($Ra$, $Nu$)

Unstable density gradient can be produced by heat. Close to the transition, this
instability leads to the formation of organized geometric structures.

```{figure} ../figs/rayleighbenard_pan.jpg
---
width: 40%
name: fig_rayleigh_benard
---
Experiment of thermal Convection.
```

The control parameter is the Rayleigh number

$$Ra = \frac{g\beta}{\nu\alpha} \Delta T H^3,$$

where $\beta$ is the thermal expansion coefficient, $\alpha$ is the thermal
diffusivity, $\Delta T$ is the difference between the bottom and top temperature
and $H$ is the height.

A effect of the development of the Rayleigh-Benard instability is to increase the
heat flux. When the Rayleigh number is large, convection is more efficient to
transport heat than conduction. The heat flux normalized by the diffusive heat
flux is called the Nusselt number $Nu$.

The plate tectonics is due to the convection of the mantle!

### Stable stratification, Kelvin-Helmoltz instability and Richardson number

When the density decreases with altitude, the density profile is stable. Such
stable density profile can influence the flow.

In particular, a shear layer that would be unstable to the shear instability can
become stable because of the density stratification. The effect of the
stratification can be quantified with the Richardson number

$$Ri = \Big(\frac{N}{d_zU}\Big)^2.$$

The condition $Ri < 1/4$ somewhere in the flow is a necessary but not sufficient
condition for the shear instability of a steady parallel inviscid shear flow
{cite:p}`Miles1961, Howard1961`.
