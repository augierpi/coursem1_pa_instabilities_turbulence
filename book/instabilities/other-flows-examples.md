# Other flows examples

## Boundary layer on a flat solid

```{figure} ../figs/instability_flat_boudary_layer.png
---
width: 45%
name: fig_instability_flat_boudary_layer
---
Sketch of the instabilities on a flat boundary layer.
```

## Boundary layer on a curvy solid (centrifugal and detachment)

- concave curve boundary: potentially unstable to the centrifugal instability.

- convex curve boundary: separation (pressure gradient).

```{figure} ../figs/separation_boundary_layer.png
---
width: 40%
name: fig_separation_boundary_layer
---
Laminar and turbulence boundary layer on a curved solid.
```

## Vortex instabilities

```{figure} ../figs/wake_aircraft.jpg
---
width: 35%
name: fig_wake_aircraft
---
Instabilities can develop in the vortices behind an
aircraft.
```

```{figure} ../figs/crow_instability.jpg
---
width: 40%
name: fig_crow_instability
---
Instabilities can develop in the vortices behind an aircraft.
```

## Poiseuille (plan and tube)

## Couette flow, Taylor-Couette flow
