# Two concepts to go further...

## Drag coefficient

The ith component of the force on an obstacle can be written as

$$F_i = \iint_\Sigma \sigma_{ij} dS_j.$$

The total power injected in the flow is

$$P = {\boldsymbol{F}}\cdot {\boldsymbol{U}},$$

where ${\boldsymbol{U}}$ is the velocity relative to the object.

We define $S$ as the surface projected on a cross-section perpendicular to the
direction of the flow. Since the strength should scale as $\rho U^2 S$, we define
the drag coefficient (dimensionless) as:

$$C_D = \frac{2F_x}{\rho U^2 S}$$

The exact definition of the surface depends on the shape of the object (sphere,
plate, etc.). The force can be due to pressure (force normal to the surface) or to
friction (viscous force).

```{figure} ../figs/drag_coef_def.png
---
width: 60%
name: fig_drag_coef_def
---
Definition of the surface involved in the definition of the drag
coefficient for different
objects.
```

For tubes or plates, an alternative quantity $\lambda$ is used to quantify the
drag. The pressure is measured at two different points and the difference of
pressure is a direct measure of the friction:

$$\lambda = \frac{2}{\rho U^2 S} \frac{\Delta P D}{l},$$

where $D$ is the diameter of the tube. Show that in the case of the tube,
$C_D
= \lambda/4$.

```{figure} ../figs/drag_coef_def_pressure_drop.png
---
width: 50%
name: fig_drag_coef_def_pressure_drop
---
The difference of pressure is measured by the difference of water height in two
tubes.
```

```{figure} ../figs/drag_coef_vs_Re_cylinder.png
---
width: 60%
name: fig_drag_coef_vs_Re_cylinder
---
Evolution of the drag coefficient with the Reynolds number for a
cylinder. The flow is stationary for small $Re$. The wake becomes
periodic for intermediate
$Re$.
```

For the cylinder the drag becomes constant for very large Reynolds number. This
implies that the energy dissipation scales like $\rho U^3/D$ and does not depend
on the viscosity.

## Amplitude equations

For some instabilities, it is possible to proposed a model describing some
characteristics of the system close to the threshold of the instability without
considering the fundamental equation of the fluid mechanics. This approach is
called the amplitude equation and is based on the symmetries of the system.

One of the simplest examples is the case of the cylinder wake (Landau model, see
p. 111 of GHP). We look for the equation describing the evolution of the complex
amplitude of the perturbation $A(t)$. We try

$$d_t |A|^2 = 2\sigma_r |A|^2 - 2B |A|^4,$$

which is equivalent to

$$d_t |A| = \sigma_r |A| - B |A|^3.$$

So a consistent equation for the complex amplitude can be

$$d_t A = \sigma A - B |A|^2 A.$$

From this equation, we find that $|A_{eq}| = \sqrt{\sigma_r / B}$. Since we know
that $\sigma_r = 0$ for $Re = Re_c$, we can use the Taylor expansion
$\sigma_r \propto (Re - Re_c)$, which would imply that
$|A_{eq}| \propto \sqrt{Re - Re_c}$. These relations are consistent with the
measurements.

This model is also consistent with the observation that the characteristic time of
the evolution of the perturbation scales like $1/(Re - Re_c)$. This time diverges
when the Reynolds number approaches the critical Reynolds number $Re_c$. These
characteristics are also observed in phase transitions, which can be described
with the same Landau model.
