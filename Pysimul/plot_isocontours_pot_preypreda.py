from pathlib import Path

import numpy as np
import matplotlib.pyplot as plt

from fluidsim.solvers.models0d.predaprey.solver import Simul

from util import savefig

params = Simul.create_default_params()


xs = np.linspace(0.01, 5, 100)
ys = np.linspace(0.01, 5, 100)

Xs, Ys = np.meshgrid(xs, ys)

potential = (
    params.C * np.log(Xs) - params.D * Xs + params.A * np.log(Ys) - params.B * Ys
)

fig, ax = plt.subplots()

ax.contour(Xs, Ys, potential, 10)

ax.set_xlabel("$X$")
ax.set_ylabel("$y$")

ax.set_title("Isolines of potential for the prey-predator model")

savefig(fig, "fig_isocontour.png")

plt.show()
